# rustybox

Rustybox adds the missing parts to make a usefull rust only minimal Linux distribution. It will never be complete and really is just a comenion for the great [rust coreutils](https://github.com/uutils/coreutils). It is Linux only.

## Scope of this project

This projet is Linux only and just a comenion for the great [rust coreutils](https://github.com/uutils/coreutils). It only contains the bare minimum tools missing to have a working Linux system. All parts which are available anywhere else will not be included.

## Fair warning

This is my first rust project so don't expect great rust code :D. Please feel free to point at things which are not great.

## Contributing

Feel free to contribute.

## License

This project is GPLv2.
