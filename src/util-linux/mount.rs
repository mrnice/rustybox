use std::env::args;
extern crate nix;
use nix::mount::mount;
use nix::mount::MsFlags;

// We want to be able to parse this
// mount -n -t proc proc /proc
struct ParsingError;

fn new_mount_point(source: String, target: String, fstype: String) -> MountPoint {
    return MountPoint{source, target, fstype};
}

struct MountPoint {
    source: String,
    target: String,
    fstype: String,
}

fn print_usage() {
    println!("\nUsage:");
    println!("mount [options] <source> <directory>");
    println!("\nMount a filesystem.\n");
    println!("Options:");
    println!("-n       don't write to /etc/mtab (default and ignored)");
    println!("-t       type of filesystem (required)");
}

fn process_args() -> Result<MountPoint, ParsingError> {
    let mut source: &String = &String::new();
    let mut target: &String = &String::new();
    let mut fstype: &String = &String::new();
    let myargs: Vec<String> = args().collect();
    println!("{:?}", myargs);
    if myargs.len() > 6 || myargs.len() < 5 {
        print_usage();
    }

    let mut i = 1;
    while i < myargs.len() {
        if myargs[i] == "-n" {
            i += 1;
            continue;
        }
        if myargs[i] == "-t" {
            if i+1 >= myargs.len() {
                print_usage();
                return Err(ParsingError);
            }
            fstype = myargs.get(i+1).unwrap();
            i += 1;
            continue;
        }
        if source.is_empty() {
            source = myargs.get(i).unwrap();
            i += 1;
            continue;
        }
        else {
            if i != myargs.len()-2 {
                print_usage();
                return Err(ParsingError);
            }
            target = myargs.get(i).unwrap();
            break;
        }
    }

    if source.is_empty() || target.is_empty() || fstype.is_empty() {
        print_usage();
        return Err(ParsingError);
    }

    // FIMXE: this looks bad. Can we elise the string?
    return Ok(new_mount_point(source.to_string(), target.to_string(), fstype.to_string()));
}

fn get_default_flags(fstype: &String) -> MsFlags {
    match fstype.as_str() {
        "proc" => return MsFlags::MS_NOSUID | MsFlags::MS_NODEV | MsFlags::MS_NOEXEC | MsFlags::MS_RELATIME,
        "sysfs" => return MsFlags::MS_NOSUID | MsFlags::MS_NODEV | MsFlags::MS_NOEXEC | MsFlags::MS_RELATIME,
        "devtmpfs" => return MsFlags::MS_NOSUID | MsFlags::MS_RELATIME,
        _ => return MsFlags::empty(),
    }
}

fn mymount(mount_point: &MountPoint) {
    let options = get_default_flags(&mount_point.fstype);
    let _ = mount(Some(mount_point.source.as_str()), mount_point.target.as_str(), Some(mount_point.fstype.as_str()), options, Some("")).expect("mount failed");
}

fn main() {
    let res = process_args();
    let res: MountPoint = match res {
        Ok(mount_point) => mount_point,
        Err(ParsingError) => panic!("Wrong options"),
    };

   mymount(&res);
}
