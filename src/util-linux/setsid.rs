// This setsid implementation is inspired from util-linux setsid
// which is licensced under public domain

use std::env::args;
use std::ffi::{CString};

fn print_usage() {
    println!("\nUsage:");
    println!("setsid program [arguments]");
}

fn main() {
    let myargs: Vec<String> = args().collect();

    if myargs.len() < 2 {
        print_usage();
        return; //FIXME: consider exit with failure
    }

    let mut cargs: Vec<CString> = vec![];

    let mut i = 1;
    while i < myargs.len() {
        cargs.push(CString::new(myargs[i].as_str()).unwrap());
        i=i+1;
   }

    if nix::unistd::getpid() == nix::unistd::getpgrp() {
        unsafe {
            let pid = libc::fork();
            match pid {
                -1 => libc::_exit(libc::EXIT_FAILURE),
                0 => {}, // child, nothing to do
                _ => libc::_exit(libc::EXIT_SUCCESS),
            }
        }
    }
    // This should be safe now
    nix::unistd::setsid().expect("setsid failed");
    nix::unistd::execvp(&cargs[0], &&cargs[..]);
}
