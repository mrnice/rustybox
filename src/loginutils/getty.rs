// This getty implementation is heavily inspired by busybox getty.
// Most coments are stolen from busybox getty as well.
// which is licensced under GPLv2 or later
// TODO: rustify this code, currently it is more or less C style

use std::env::args;
use std::os::raw::{c_char};
use std::ffi::{CString};
use std::io::Write;
use nix::unistd;

struct ParsingError;

fn new_getty(tty: String, baudrate: u32) -> Getty {
    return Getty{tty, baudrate};
}

struct Getty {
    tty: String,
    baudrate: u32,
}

fn print_usage() {
    println!("\nUsage:");
    println!("getty BAUDRATE(ignored) TTY");
}

fn process_args() -> Result<Getty, ParsingError> {
    let myargs: Vec<String> = args().collect();
    println!("{:?}", myargs);
    if myargs.len() != 3 {
        return Err(ParsingError);
    }

    // FIXME: add /dev/
    // FIMXE: as we only use it as CString convert it here and change the Getty
    let tty = match myargs.get(2) {
        Some(tty) => tty,
        None => return Err(ParsingError),
    };

    let baudrate_arg = match myargs.get(1) {
        Some(baudrate_arg) => baudrate_arg,
        None => return Err(ParsingError),
    };

    let baudrate = match u32::from_str_radix(baudrate_arg, 8) {
        Ok(baudrate) => baudrate,
        Err(_) => return Err(ParsingError),
    };
    return Ok(new_getty(tty.to_string(), baudrate));
}

fn ndelay_off(fd: std::os::fd::RawFd) {
    // FIMXE: find out why the ::from_bits fails it contains valid flags :/
    //let res = nix::fcntl::fcntl(fd, nix::fcntl::F_GETFL).expect("get fcntl failed");
    //let flags = nix::fcntl::OFlag::from_bits(res).expect("from bits failed");
    //nix::fcntl::fcntl(fd, nix::fcntl::F_SETFL(!nix::fcntl::OFlag::O_NONBLOCK & flags)).expect("set fcntl failed");
    unsafe {
        let res = libc::fcntl(fd, libc::F_GETFL);
        let bitmask = res & libc::O_NONBLOCK;
        match bitmask {
            libc::O_NONBLOCK => {
                libc::fcntl(fd, libc::F_SETFL, res & !libc::O_NONBLOCK);
            },
            _ => return,
        }
    }
}

fn open_tty(tty: &String)
{
    unsafe{
        libc::close(0);
        let tty_dev: CString = CString::new(tty.as_str()).unwrap();
        let fd = libc::open(tty_dev.as_ptr(), libc::O_RDWR | libc::O_NONBLOCK);
        if fd < 0 {
            panic!("open failed");
        }
        libc::fchown(0, 0, 0);
        libc::fchmod(0, 0620);
    }
}

// FIXME: split and rustify this function, but first make it work
fn getty(getty: &Getty) {
    //let pid = match unistd::setsid() {
    //    Ok(Pid) => Pid, //println!("{Pid}"),
    //    Err(_) if unistd::getpid() == unistd::getsid(Some(unistd::Pid::from_raw(0))).unwrap() => unistd::getpid(),
    //    Err(_) => panic!("setsid"),
    //};
    //println!("pid: {pid}");

    // TODO: rustify this code ^^ but it is a nice experience to write C in rust :D
    unsafe {
        let mut pid = libc::setsid();
        println!("pid after setsid: {pid}");
        if pid < 0 {
            pid = libc::getpid();
            println!("pid after getpid: {pid}");
            let sid = libc::getsid(0);
            println!("sid after getsid(0): {sid}");
            if sid != pid {
                panic!("error setsid");
            }

            /* Looks like we are already a session leader.
             * In this case (setsid failed) we may still have ctty,
             * and it may be different from tty we need to control!
             * If we still have ctty, on Linux ioctl(TIOCSCTTY)
             * (which we are going to use a bit later) always fails -
             * even if we try to take ctty which is already ours!
             * Try to drop old ctty now to prevent that.
             * Use O_NONBLOCK: old ctty may be a serial line.
             */
            let dev_tty: CString = CString::new("/dev/tty").unwrap();
            let fd = libc::open(dev_tty.as_ptr(), libc::O_RDWR | libc::O_NONBLOCK);
            if fd >= 0 {
                /* TIOCNOTTY sends SIGHUP to the foreground
                 * process group - which may include us!
                 * Make sure to not die on it:
                 */
                let old = libc::signal(libc::SIGHUP, libc::SIG_IGN);
                libc::ioctl(fd, libc::TIOCNOTTY);
                libc::close(fd);
                libc::signal(libc::SIGHUP, old);
            }

            //* Close stdio, and stray descriptors, just in case */
            //let dev_null: CString = CString::new("/dev/null").unwrap();
            //let mut n = libc::open(dev_null.as_ptr(), libc::O_RDWR);
            //libc::dup2(n, 1);
            //libc::dup2(n, 2);
            //while n > 2 {
            //    nix::unistd::close(n);
            //    n -= 1;
            //}
        }

        //FIXME: this could be save but we want pid in our scope
        //       split the unsafe parts in functions ...
        open_tty(&getty.tty);
        let _ = ndelay_off(libc::STDIN_FILENO);

        if unistd::dup2(libc::STDIN_FILENO, 1).expect("dup2 1 failed") != 1 {
            panic!("dup2 failed");
        }
        if unistd::dup2(libc::STDIN_FILENO, 2).expect("dup2 2 failed") != 2 {
            panic!("dup2 failed");
        }

        /* Steal ctty if we don't have it yet */
        let tsid = libc::tcgetsid(libc::STDIN_FILENO);
        if tsid < 0 || pid != tsid {
            if libc::ioctl(libc::STDIN_FILENO, libc::TIOCSCTTY, 1) < 0 {
                panic!("TIOSCTTY");
            }
        }

        /* Make ourself a foreground process group within our session */
        if libc::tcsetpgrp(libc::STDIN_FILENO, pid) < 0 {
            panic!("tcsetpgrp");
        }

        /*
         * The following ioctl will fail if stdin is not a tty, but also when
         * there is noise on the modem control lines. In the latter case, the
         * common course of action is (1) fix your cables (2) give the modem more
         * time to properly reset after hanging up. SunOS users can achieve (2)
         * by patching the SunOS kernel variable "zsadtrlow" to a larger value;
         * 5 seconds seems to be a good value.
         */
        let _ = nix::sys::termios::tcgetattr(std::io::stdin()).expect("tcgetattr failed");

        // FIXME: update_utmp -> we want to use rustyd should we support SysV init?

        // OK: we now officialy own tty and are ready to go :)
        // FIXME: sett tty options and speed
    }
}

fn get_login_name() -> CString {
    let hostname: String = match nix::unistd::gethostname() {
        Ok(hostname) => match hostname.into_string() {
            Ok(s) => s,
            Err(_) => "host is not UTF-8".to_string(),
        },
        Err(_) => "nohost".to_string(),
    };
    print!("{hostname} login: ");

    // Well well well don't panic :D
    // FIXME: return an Err and retry the get_login_name
    std::io::stdout().flush().unwrap();
    return CString::new(std::io::stdin().lines().next().unwrap().unwrap()).unwrap();
}

fn print_issue() {
    let issue: String = match std::fs::read_to_string("/etc/issue") {
        Ok(issue) => issue, // FIXME: parse issue file and replace the tokens
        Err(_) => "No /etc/issue found".to_string(),
    };
    println!("{issue}");
}

fn main() {
    let res = process_args();
    let res: Getty = match res {
        Ok(getty) => getty,
        Err(ParsingError) => {
            print_usage();
            panic!("Wrong options");
        },
    };

    getty(&res);
    print_issue();
    let login_name = get_login_name();
    println!("try to start login");

    let cargs: Vec<CString> = vec![
        CString::new("/bin/login").unwrap(),
        CString::new("--").unwrap(),
        login_name
    ];
    unistd::execvp(&cargs[0], &&cargs[..]);
}
