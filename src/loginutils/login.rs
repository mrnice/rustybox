// FIXME: currently this is only a fake login to start the ion shell
//        and to help to write getty first.

use std::ffi::CString;
use nix::unistd;
use std::env::args;

fn main() {
    let myargs: Vec<String> = args().collect();
    println!("{:?}", myargs);
    println!("Login sucess :D welcome root, I will start your shell");
    unistd::execv(&(CString::new("/usr/bin/ion").unwrap()), &[CString::new("ion").unwrap()]).expect("execv failed");
}
