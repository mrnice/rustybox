// Quckly hacked together chmod
// Please don't use it of anything serious as it doesn't do ANY checks.
//
// FIMXE: make this at least a bit more robust
// NOTE: to my self rust helps you but you can still shoot you in the foot

use std::env::args;
use std::os::raw::{c_char};
use std::ffi::{CString};

struct ParsingError;

fn new_chmod(filename: String, mode: u32) -> Chmod {
    return Chmod{filename, mode};
}

struct Chmod {
    filename: String,
    mode: u32,
}

fn print_usage() {
    println!("\nUsage:");
    println!("chmod [OPTION]... OCTAL-MODE FILE");
    println!("\nChange the mode of FILE to MODE.\n");
}

fn process_args() -> Result<Chmod, ParsingError> {
    let myargs: Vec<String> = args().collect();
    println!("{:?}", myargs);
    if myargs.len() != 3 {
        print_usage();
    }

    let filename: &String = myargs.get(2).unwrap();
    let mode: u32 = u32::from_str_radix(myargs.get(1).unwrap(), 8).unwrap();
    // FIMXE: error handling
    return Ok(new_chmod(filename.to_string(), mode));
}

fn main() {
    let res = process_args();
    let res: Chmod = match res {
        Ok(chmod) => chmod,
        Err(ParsingError) => panic!("Wrong options"),
    };

    // My first unsafe, a what a wonderful feeling :D
    unsafe {
        let s: CString = CString::new(res.filename.as_str()).unwrap();
        let f: *const c_char = s.as_ptr();
        let ret = libc::chmod(f, res.mode);
    }
}
