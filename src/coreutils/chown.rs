use std::env::args;
use passwd::Passwd;
use std::os::unix::fs::chown;

// FIMXE: find out how to use a delimiter on positional args in clap
//        and replace the manual parsing
struct ParsingError;

fn new_chown(filename: String, owner: String, group: String) -> Chown {
    return Chown{filename, owner, group};
}

struct Chown {
    filename: String,
    owner: String,
    group: String, // FIMXE: use Option
}

fn print_usage() {
    println!("\nUsage:");
    println!("chown [OWNER][:[GROUP] FILE");
    println!("\nChange the owner and/or group of FILE to OWNER and/or GROUP.\n");
}

fn process_args() -> Result<Chown, ParsingError> {
    let myargs: Vec<String> = args().collect();
    println!("{:?}", myargs);
    if myargs.len() != 3 {
        print_usage();
        return Err(ParsingError);
    }

    let filename: &String = myargs.get(2).unwrap();
    let owner_group: Vec<&str> = myargs.get(1).unwrap().split(":").collect();
    let owner = owner_group.get(0).unwrap_or_else(|| &"");
    let group = owner_group.get(1).unwrap_or_else(|| &"");
    if owner.is_empty() {
        println!("group only is currently not implemented");
        return Err(ParsingError);
    }
    return Ok(new_chown(filename.to_string(), owner.to_string(), group.to_string()));
}

fn main() {
    let res = process_args();
    let res: Chown = match res {
        Ok(chown) => chown,
        Err(ParsingError) => panic!("Wrong options"),
    };

    let passwd = Passwd::from_name(&res.owner).expect("user not found!");
    // FIXME: implement group
    chown(res.filename, Some(passwd.uid), None).expect("error");
}
