//! This modules provides a wrapper around the libc functions in `grp.h` for
//! handling the `/etc/group` file, which stores information about groups.
//!
//! # Examples
//!
//! ```
//! println!("{:?}", Passwd::from_name("root"));
//!
//! Based on passwd https://github.com/ids1024/passwd-rs which is MIT licensed
//! ```


extern crate libc;

use std::ffi::CString;
use std::ffi::CStr;

/// Represents an entry in `/etc/group`
#[derive(Debug)]
pub struct Group {
    /// username
    pub name: String,
    /// user password
    pub password: String,
    /// group ID
    pub gid: libc::gid_t,
    // FIXME: gr_mem NULL-terminated array of pointers to names of group members
}

impl Group {
    unsafe fn from_ptr(grp: *const libc::group) -> Group {
        Group {
            name: CStr::from_ptr((*grp).gr_name).to_str().unwrap().to_owned(),
            password: CStr::from_ptr((*grp).gr_passwd).to_str().unwrap().to_owned(),
            gid: (*grp).gr_gid,
        }
    }

    /// Gets a `Group` entry for the given groupname, or returns `None`
    pub fn from_name(group: &str) -> Option<Group> {
        let c_group = CString::new(group).unwrap();

        let mut pwd: libc::group = unsafe { std::mem::zeroed() };
        let mut buf = Vec::with_capacity(getgr_r_size_max());
        let mut result = std::ptr::null_mut();
        unsafe {
            libc::getgrnam_r(c_group.as_ptr(),
                             &mut pwd,
                             buf.as_mut_ptr(),
                             buf.capacity(),
                             &mut result);
        }

        if result.is_null() {
            None
        } else {
            Some(unsafe { Group::from_ptr(result) })
        }
    }
}

fn getgr_r_size_max() -> usize {
    // Borrowed from libstd/sys/unix/os.rs
    // (As are a few lines elsewhere)
    match unsafe { libc::sysconf(libc::_SC_GETGR_R_SIZE_MAX) } {
        n if n < 0 => 512 as usize,
        n => n as usize,
    }
}
